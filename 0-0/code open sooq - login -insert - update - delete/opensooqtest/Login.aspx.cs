﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace opensooqtest
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Entities db = new Entities();
            try
            {

                string username = txtUsername.Text;
                string password = txtPassword.Text;

                var Query = from users in db.tbl_users
                            where users.username == username && users.password == password
                            select users;
                tbl_users objCheck = Query.Single();//row already exist
                Session["username"] = txtUsername.Text;
                Response.Redirect("Admin/Admin_user2.aspx");
            }
            catch (Exception ex)
            {
                lblInfo.Text = "failed";
            }
        }
    }
}