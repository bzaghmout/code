﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace opensooqtest.Admin
{
    public partial class Admin_user2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                MultiView1.SetActiveView(View1);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(View2);
            hdnID.Value = "0"; //add
            EmptyTextBox();
        }

        private void EmptyTextBox()
        {
            txtconfirmpassword.Text = "";
            txtEmail.Text = "";
            txtFullname.Text = "";
            txtMobile.Text = "";
            txtPassword.Text = "";
            txtUsername.Text = "";


        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "edit_user")
            {
                int UserID = Convert.ToInt32(e.CommandArgument.ToString());
                hdnID.Value = UserID.ToString();
                Entities db = new Entities();
                var Query = from u in db.tbl_users
                            where u.user_id == UserID
                            select u;
                tbl_users obj = Query.Single();
                txtFullname.Text = obj.full_name;
                txtUsername.Text = obj.username;
                txtPassword.Text = obj.password;
                txtconfirmpassword.Text = obj.password;
                txtMobile.Text = obj.mobile;
                txtEmail.Text = obj.email;

                MultiView1.SetActiveView(View2);



            }

            if (e.CommandName == "delete_user")
            {
                int UserID = Convert.ToInt32(e.CommandArgument.ToString());

                Entities db = new Entities();
                var Query = from users in db.tbl_users
                            where users.user_id == UserID
                            select users;
                tbl_users obj = Query.Single();
                db.tbl_users.Remove(obj);
                db.SaveChanges();
                GridView1.DataBind();

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdnID.Value == "0")
            {
                Entities db = new Entities();

                try
                {
                    string username = txtUsername.Text;
                    string email = txtEmail.Text;

                    var Query = from users in db.tbl_users
                                where users.username == username || users.email == email
                                select users;
                    tbl_users objCheck = Query.Single();//row already exist
                    lblInfo.Text = "User name or email alrealy exist";

                }
                catch (Exception ex)
                {
                    //insert
                    tbl_users obj = new tbl_users();//new row from tbl_users    
                    obj.username = txtUsername.Text;
                    obj.full_name = txtFullname.Text;
                    obj.password = txtPassword.Text;
                    obj.email = txtEmail.Text;
                    obj.mobile = txtMobile.Text;
                    obj.status = chkStatus.Checked;
                    if (RadioButtonList1.SelectedValue == "0")
                        obj.gender = false;
                    else
                        obj.gender = true;

                    db.tbl_users.Add(obj);
                    db.SaveChanges();

                    MultiView1.SetActiveView(View1);
                    GridView1.DataBind();

                }



            }
            else
            {
                //update

                Entities db = new Entities();
                int UserID = Convert.ToInt32(hdnID.Value);

                var Query = from users in db.tbl_users
                            where users.user_id == UserID
                            select users;
                tbl_users obj = Query.Single();

                obj.username = txtUsername.Text;
                obj.full_name = txtFullname.Text;
                obj.password = txtPassword.Text;
                obj.email = txtEmail.Text;
                obj.mobile = txtMobile.Text;
                obj.status = chkStatus.Checked;
                if (RadioButtonList1.SelectedValue == "0")
                    obj.gender = false;
                else
                    obj.gender = true;


                db.SaveChanges();

                MultiView1.SetActiveView(View1);
                GridView1.DataBind();


            }
        }
    }
    }
