﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace opensooqtest
{
    public partial class Admin_user : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection();
        SqlCommand cmd=new SqlCommand();
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MultiView1.SetActiveView(View1);
            }
           
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            

            if(e.CommandName=="user_delete")
            {


                int UserIDD = Convert.ToInt32(e.CommandArgument.ToString());

                con = new SqlConnection();
                con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConStr1"].ToString();
                con.Open();
                
                cmd =new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "sp_user_delete";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@user_id", UserIDD);
                cmd.ExecuteNonQuery();
                con.Close();
                GridView1.DataBind();
                
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(View2);
            hdnID.Value = "0";//add
            EmptyTextBox();

        }

        private void EmptyTextBox()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtconfirmpassword.Text = "";
            txtFullname.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
           
        }

  

        protected void Button4_Click(object sender, EventArgs e)
        {
            EmptyTextBox();
            MultiView1.SetActiveView(View1);

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
    }

}