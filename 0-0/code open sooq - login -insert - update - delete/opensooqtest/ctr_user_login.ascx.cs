﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace opensooqtest
{
    public partial class ctr_user_login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null || Session["username"].ToString() == "")
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                string username = Session["username"].ToString();
                Entities db = new Entities();
                var Query = from users in db.tbl_users
                            where users.username == username
                            select users;
                tbl_users obj = Query.Single();
                lblUsername.Text = obj.full_name;
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("../Login.aspx");
        }
    }
}