﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="patient.aspx.cs" Inherits="Patientproject.Page.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/table.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table <%--class="table-patient"--%> class="table table-striped">
        <h1>Patient</h1>
        <tr>
            <td class="td-id">ID</td>
            <td>Name</td>
            <td>Email</td>
            <td>Gender</td>
            <td>DOB</td>
            <td>Active</td>
            <td>Creation</td>
        </tr>
        
        <tr>
            <td class="td-id">1</td>
            <td>Anas Qutaishat</td>
            <td>anass.shadded@gmail.com</td>
            <td>male</td>
            <td>1/1/2015</td>
            <td>true</td>
            <td>by Anas Qutaishat at 1/1/2015</td>
        </tr>


        <tr>
            <td class="td-id">2</td>
            <td>Basel K. Zaghmout</td>
            <td>basel.kzaghmout@gmail.com</td>
            <td>male</td>
            <td>26/8/2018</td>
            <td>true</td>
            <td>by basel zaghmout at 26/8/2018</td>
        </tr>
    </table>

    <a href="PatientEdit.aspx"><button class="btn btn-default btn-lg" type="button" id="btnAdd"  onclick="addbuttoun()" >Add</button></a>

</asp:Content>
