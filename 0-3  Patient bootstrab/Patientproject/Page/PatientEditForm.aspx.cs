﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Patientproject
{
    public partial class PatientEditForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int patientID = int.Parse((Request.QueryString["id"]));

                if (patientID == 0)
                {
                    btnDelet.Visible = false;

                }
                else
                { 
                    Entities1 db = new Entities1();
                    var Query = from patient in db.tbl_patient
                                where patient.id == patientID
                                select patient;
                    tbl_patient obj = Query.Single();
                    txtUsername.Text = obj.fname;
                    txtMiddlename.Text = obj.mname;
                    txtLastname.Text = obj.lname;
                    txtEmail.Text = obj.email;

                    if (RadioButtonList1.SelectedValue == "0")
                        obj.gender = false;
                    else
                        obj.gender = true;


                    var Query1 = from patient in db.tbl_patient
                                 select patient;
                    List<tbl_patient> lst = Query1.ToList();
                    ddlPatient.DataSource = lst;
                    ddlPatient.DataTextField = "status";
                    ddlPatient.DataValueField = "id";
                    ddlPatient.DataBind();
                    db.SaveChanges();
                }


            }

        }

        protected void btnDelet_Click(object sender, EventArgs e)
        {

            int patientID = int.Parse((Request.QueryString["id"]));

            Entities1 db = new Entities1();
            var Query = from patient in db.tbl_patient
                        where patient.id == patientID
                        select patient;
            tbl_patient obj = Query.Single();
            db.tbl_patient.Remove(obj);
            db.SaveChanges();
            Response.Redirect("PatientDatabase.aspx");

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("PatientDatabase.aspx");
        }

        
        protected void Button1_Click(object sender, EventArgs e)
        {

            int patientID = int.Parse((Request.QueryString["id"]));
            if (patientID == 0)
            {
           

                Entities1 db = new Entities1();
                tbl_patient obj = new tbl_patient();//new row from tbl_users    
                obj.fname = txtUsername.Text;
                obj.mname = txtMiddlename.Text;
                obj.lname = txtLastname.Text;
                obj.email = txtEmail.Text;
                if (RadioButtonList1.SelectedValue == "0")
                    obj.gender = false;
                else
                    obj.gender = true;
                db.tbl_patient.Add(obj);
                db.SaveChanges();
                Response.Redirect("PatientDatabase.aspx");
            }
            else
            {
                

                Entities1 db = new Entities1();
                var Query = from user in db.tbl_patient
                            where user.id == patientID
                            select user;
                tbl_patient obj = Query.Single();
                obj.fname = txtUsername.Text;
                obj.mname = txtMiddlename.Text;
                obj.lname = txtLastname.Text;
                obj.email = txtEmail.Text;
                if (RadioButtonList1.SelectedValue == "0")
                    obj.gender = false;
                else
                    obj.gender = true;


                db.SaveChanges();

                Response.Redirect("PatientDatabase.aspx");
               
            }
        }
    }
}
