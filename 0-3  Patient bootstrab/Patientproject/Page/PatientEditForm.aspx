﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="PatientEditForm.aspx.cs" Inherits="Patientproject.PatientEditForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="CSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <form id="form1" runat="server" class="form-horizontal col-lg-7" >

        <div>
            <h1>New patient</h1>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Username</label>

            <div class="col-sm-7">
                <asp:TextBox ID="txtUsername" runat="server" class="form-control" placeholder="Username" ></asp:TextBox>
                </div>

            <div  class="col-sm-3" >
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername" ErrorMessage="Enter your name" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
        
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">Middle name</label>
            <div class="col-sm-7">
                <asp:TextBox ID="txtMiddlename" runat="server" class="form-control" placeholder="Middlename"></asp:TextBox>
            </div>

            <div  class="col-sm-3" >

                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMiddlename" ErrorMessage="Enter your middle name" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>

            </div>
        </div>



        <div class="form-group">
            <label class="col-lg-2 control-label">Last name</label>
            <div class="col-lg-7">
               
                <asp:TextBox ID="txtLastname" runat="server" class="form-control" placeholder="Last name" ></asp:TextBox>
            </div>
            <div  class="col-sm-3" >

                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLastname" ErrorMessage="Enter your last name" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>

            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Last check</label>
            <div class="col-sm-7">
                 <asp:TextBox ID="txtLastcheck" runat="server" class="form-control" placeholder="Last check" ></asp:TextBox>
            </div>
            <div  class="col-sm-3" >

                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter last check" ControlToValidate="txtLastcheck" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>

            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-7">
               
                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email" ></asp:TextBox>
            </div>
            <div  class="col-sm-1.5" >

                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="Enter your email" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>

            </div>
            <div  class="col-sm-1.5" >

                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Enter the correct email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>

            </div>
        </div>






        <div class="form-group">
            <label class="col-lg-2 control-label">Status</label>
            <div class="col-lg-3">
                <asp:DropDownList ID="ddlPatient" runat="server" class="form-control" AutoPostBack="True">
                    <asp:ListItem Value="0">Draft</asp:ListItem>
                    <asp:ListItem Value="1">Save</asp:ListItem>
                    <asp:ListItem Value="2">Delete</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>


        <div class="form-group">
            <label class="col-lg-2 control-label">Gender </label>
            <div class="col-lg-10">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">Male</asp:ListItem>
                    <asp:ListItem Value="1">Female</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>


        <div class="form-group">

            <div class="col-lg-10 col-lg-offset-2">
                <asp:Button ID="btnsave" runat="server" Text="Save"  class="btn btn-default" OnClick="Button1_Click"/>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default" OnClick="btnCancel_Click" CausesValidation="False" />
                  <asp:Button ID="btnDelet" runat="server" Text="Delete" class="btn btn-default" OnClick="btnDelet_Click" CausesValidation="False" />
            &nbsp;<br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                <br />
            </div>
        </div>





    </form>


</asp:Content>
