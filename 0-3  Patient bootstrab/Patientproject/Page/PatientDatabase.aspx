﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="PatientDatabase.aspx.cs" Inherits="Patientproject.PatientDatabase" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server" class="form-horizontal col-lg-12" >

        <div class="form-group ">
            <div class="col-sm-3"><h1>Patient</h1></div>
            </div>
        <div class="form-group ">
            <div class="col-sm-1 col-lg-offset-6>">
                <asp:Button ID="btnAdd" runat="server" Text="Add"  class="btn btn-default" OnClick="btnAdd_Click" />
            </div>
        </div>


        <div class="form-group">
          
            <div class="col-sm-12">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="db_patient" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" AllowPaging="True" AllowSorting="True" OnRowCommand="GridView1_RowCommand" >
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                        <asp:BoundField DataField="fname" HeaderText="User name" SortExpression="fname" />
                        <asp:BoundField DataField="mname" HeaderText="Middel name" SortExpression="mname" />
                        <asp:BoundField DataField="lname" HeaderText="Last name" SortExpression="lname" />
                        <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                        <asp:BoundField DataField="dob" HeaderText="DOB" SortExpression="dob" />
                        <asp:CheckBoxField DataField="gender" HeaderText="Gender" SortExpression="gender" />
                        <asp:BoundField DataField="lastcheck" HeaderText="Last Check" SortExpression="lastcheck" />
                        <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                        <asp:CheckBoxField DataField="active" HeaderText="Active" SortExpression="active" />
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="edit_patient" Text="Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                <asp:SqlDataSource ID="db_patient" runat="server" ConnectionString="<%$ ConnectionStrings:db_Patient1ConnectionString %>" SelectCommand="SELECT * FROM [tbl_patient]"></asp:SqlDataSource>
                <br />
                <br />
                <asp:HiddenField ID="hdnID" runat="server" />
            </div>
        </div>

    </form>
</asp:Content>
