//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patientproject
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_patient
    {
        public int id { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public Nullable<bool> gender { get; set; }
        public Nullable<System.DateTime> lastcheck { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<bool> active { get; set; }
    }
}
