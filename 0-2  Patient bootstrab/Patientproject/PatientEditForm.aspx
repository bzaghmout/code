﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="PatientEditForm.aspx.cs" Inherits="Patientproject.PatientEditForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="CSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <form class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="txtUsername" placeholder="Username">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="txtEmail" placeholder="Email">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Middle name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="txtMiddlename" placeholder="Middlename">
            </div>
        </div>
    </form>


</asp:Content>
