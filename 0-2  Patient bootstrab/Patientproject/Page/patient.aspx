﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="patient.aspx.cs" Inherits="Patientproject.Page.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/table.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">

        <h3>Patient <span class="label label-default">New</span></h3>
        <table <%--class="table-patient"--%> class="table table-striped">

            <tr>
                <td class="td-id">ID</td>
                <td>Name</td>
                <td>Email</td>
                <td>Gender</td>
                <td>DOB</td>
                <td>Active</td>
                <td>Creation</td>
            </tr>

            <tr>
                <td class="td-id">1</td>
                <td>Anas Qutaishat</td>
                <td>anass.shadded@gmail.com</td>
                <td>male</td>
                <td>1/1/2015</td>
                <td>true</td>
                <td>by Anas Qutaishat at 1/1/2015</td>
            </tr>


            <tr>
                <td class="td-id">2</td>
                <td>Basel K. Zaghmout</td>
                <td>basel.kzaghmout@gmail.com</td>
                <td>male</td>
                <td>26/8/2018</td>
                <td>true</td>
                <td>by basel zaghmout at 26/8/2018</td>
            </tr>
        </table>

        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="patient.aspx">1</a></li>
                <li><a href="patient2.aspx">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>


        <a href="PatientEdit.aspx" class="btn btn-primary active" role="button">Add</a>

    </div>
</asp:Content>
