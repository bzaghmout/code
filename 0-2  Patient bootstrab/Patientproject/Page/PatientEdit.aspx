﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/mp1.Master" AutoEventWireup="true" CodeBehind="PatientEdit.aspx.cs" Inherits="Patientproject.Page.editpatient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="../CSS/table.css" rel="stylesheet" type="text/css" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1>Patient (New)</h1>
                
            </div>

        </div>

        <div class="row">
            <div class="col-lg-3">
                User name :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtUsername" class="form-control" />
            </div>

            <div class="col-lg-3">
                Email :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtEmail" class="form-control" />
            </div>
        </div>

        <br />


        <div class="row">
            <div class="col-lg-3">
                Middele name :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtMaddilname" class="form-control" />
            </div>

            <div class="col-lg-3">
                Data of birth :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtDataofbirth" class="form-control" />
            </div>
        </div>

        <br />


        <div class="row">
            <div class="col-lg-3">
                Last name :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtLastname" class="form-control" />
            </div>

            <div class="col-lg-3">
                Last check  :
            </div>

            <div class="col-lg-3">
                <input type="text" id="txtLastcheck" class="form-control" />
            </div>
        </div>


        <br />


        <div class="row">
            <div class="col-lg-3">
                status :
            </div>

            <div class="col-lg-3">
                <select class="form-control">
                    <option>Draft</option>
                    <option>Seved</option>
                    <option>Delete</option>
                    <option>Canceled</option>
                </select>
            </div>

            <div class="col-lg-3">
                Gender :
            </div>

            <div class="col-lg-3">
                <input type="radio" name="gender" value="0" />Male
                <input type="radio" name="gender" value="1" />Female
            </div>
        </div>


        <br />


        <div class="row">
            <div class="col-lg-3">
                Action :
            </div>

            <div class="col-lg-3">
                <input type="checkbox" />
            </div>

        </div>

        <div class="row">
            <div class="col-lg-3">
                <a href="patient.aspx" class="btn btn-default btn-lg active" role="button">Save</a>
            </div>

            <div class="col-lg-3">
                <a href="patient.aspx" class="btn btn-default btn-lg active" role="button">Cancel</a>
              

            </div>
        </div>

    </div>






</asp:Content>
